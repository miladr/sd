<?php namespace App\Infrastructures\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int       id
 * @property string    token
 * @property User|null user
 * @property Carbon    created_at
 * @property Carbon    updated_at
 */
class Token extends Model
{

    protected $fillable = [ 'id', 'token', 'user_id' ];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}