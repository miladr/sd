<?php namespace App\Http\Controllers;

use App\Services\User\Exceptions\TokenNotValidException;
use App\Services\User\IUserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserAuthController extends Controller
{

    public function getLogin(Request $request)
    {
        return view('auth.login-register');
    }


    /**
     * @param Request      $request
     * @param IUserService $userService
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postLogin(Request $request, IUserService $userService)
    {
        $this->validate($request, [
            'email'    => 'required|email',
            'password' => 'required|min:6',
        ]);

        // find user by email
        $user = $userService->getByEmail($request->input('email'));
        // if there is now user with given email, register it.
        if (is_null($user)) {
            $user = $userService->create($request->input('email'), $request->input('password'));
            $userService->attempt($user, $request->input('password'));

            return redirect()->route('dashboard');
        }

        // if there is a user with given email try to authenticate her
        $attemptResult = $userService->attempt($user, $request->input('password'));
        if ( ! $attemptResult) {
            return redirect()->route('login')->withErrors([ 'Wrong Credentials' ]);

        }

        return redirect()->route('dashboard');

    }


    /**
     * @param Request      $request
     * @param IUserService $userService
     * @param string       $token
     *
     * @return void
     */
    public function getActivate(Request $request, IUserService $userService, string $token)
    {
        try {
            $activatedUser = $userService->activateUser($token);

            // if user is logged in and it activates another user accounts logout current user.
            if ($request->user() && $request->user()->id != $activatedUser->id) {
                Auth::logout();
            }

        } catch (TokenNotValidException $e) {
            return view('errors.general')->with('message', 'Token is invalid!');
        }

        return view('auth.activation')->with('message', 'Token is invalid!');

    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getLogout(Request $request)
    {
        Auth::logout();

        return redirect()->route('login');

    }
}