<?php namespace App\Services\User;

use App\Infrastructures\Models\Token;
use App\Infrastructures\Models\User;
use App\Notifications\UserRegistered;
use App\Services\User\Exceptions\TokenNotValidException;
use App\Services\User\Exceptions\UserNotExistException;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserService implements IUserService
{

    /**
     * @var Hasher
     */
    private $hasher;


    public function __construct(Hasher $hasher)
    {

        $this->hasher = $hasher;
    }


    /**
     * @param string $email
     *
     * @return User|null
     */
    public function getByEmail(string $email): ?User
    {
        return User::where('email', $email)->first();
    }


    /**
     * @param string $email
     * @param string $password
     * @param bool   $active
     *
     * @return User
     * @throws \Exception
     */
    public function create(string $email, string $password, bool $active = false): User
    {
        try {
            DB::beginTransaction();

            $user = new User();
            $user->email = $email;
            $user->password = bcrypt($password);
            $user->active = $active;
            $user->save();

            if ( ! $user->isActive()) {
                $token = $this->createActivationToken($user->id);
                $user->notify(new UserRegistered($user, $token));
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }

        return $user;
    }


    /**
     * @param User   $user
     * @param string $password
     *
     * @return bool
     */
    public function attempt(User $user, string $password): bool
    {
        if ($this->hasher->check($password, $user->password)) {
            Auth::login($user);

            return true;
        }

        return false;
    }


    /**
     * Create a token for user
     *
     * @param int $userId
     * @param int $length
     *
     * @return Token
     */
    private function createActivationToken(int $userId, int $length = 12): Token
    {
        return Token::create([ 'user_id' => $userId, 'token' => str_random($length) ]);
    }


    /**
     * @param string $token
     *
     * @return User
     * @throws TokenNotValidException
     * @throws \Exception
     */
    public function activateUser(string $token): User
    {

        /** @var Token $token */
        $token = Token::where('token', $token)->first();
        if (is_null($token)) {
            throw new TokenNotValidException();
        }
        /** @var User $user */
        $user = $token->user;

        try {
            DB::beginTransaction();
            $user->active = true;
            $user->save();
            $token->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }

        return $user;

    }

}