<?php namespace App\Services\User;

use App\Infrastructures\Models\User;

interface IUserService
{

    /**
     * Retrieve user by email
     *
     * @param string $email
     *
     * @return User
     */
    public function getByEmail(string $email): ?User;


    /**
     * Create a new User
     *
     * @param string $email
     * @param string $password
     * @param bool   $active
     *
     * @return User
     */
    public function create(string $email, string $password, bool $active = false): User;


    /**
     * Attempt  to login user
     *
     * @param User   $user
     * @param string $password
     *
     * @return bool
     */
    public function attempt(User $user, string $password): bool;


    /**
     * @param string $token
     *
     * @return User
     */
    public function activateUser(string $token): User;

}