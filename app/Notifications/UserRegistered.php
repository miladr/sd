<?php

namespace App\Notifications;

use App\Infrastructures\Models\Token;
use App\Infrastructures\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class UserRegistered extends Notification
{

    use Queueable;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Token
     */
    private $token;


    /**
     * Create a new notification instance.
     *
     * @param User  $user
     * @param Token $token
     */
    public function __construct(User $user, Token $token)
    {
        $this->user = $user;
        $this->token = $token;
    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [ 'mail' ];
    }


    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return ( new MailMessage )->line('Account Activation')->action('Activate your account',
            route('user-activation', [ 'token' => $this->token->token ]))->line('Thank you for using our application!');
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [//
        ];
    }
}
