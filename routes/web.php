<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
//Route::get('/', function () {
//    return view('welcome');
//});

Route::middleware([ 'guest' ])->group(function () {
    Route::get('login', 'UserAuthController@getLogin')->name('login');
    Route::post('login', 'UserAuthController@postLogin')->name('login');
});

Route::get('/activate/{token}', 'UserAuthController@getActivate')->name('user-activation');

Route::middleware([ 'auth' ])->group(function () {
    Route::get('/', 'DashboardController@getDashboard')->name('dashboard');
    Route::get('/logout', 'UserAuthController@getLogout')->name('logout');
});

