@extends('layouts.base')

@section('title', 'Login or Register')

@section('content')
    <div class="row  justify-content-center align-items-center">
        <div class="col-6">
            <h1>Hooray, Your account is active now. Go to <a href="{{route('dashboard')}}">Dashboard</a></h1>
        </div>
    </div>
@endsection
