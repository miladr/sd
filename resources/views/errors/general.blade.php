@extends('layouts.base')

@section('title', 'Login or Register')

@section('content')
    <div class="row  justify-content-center align-items-center">
        <div class="col-6">
            <div class="alert alert-danger"><h3>{{ $message }}</h3></div>
        </div>
    </div>
@endsection
