@extends('layouts.base')

@section('title', 'Login or Register')

@section('content')
    <div class="row  justify-content-center align-items-center">
        <div class="col-6">
            <h3>Hello {{ $user->email }}</h3>
            <table class="table">
                <tr>
                    <td>ID: </td>
                    <td>{{ $user->id }}</td>
                </tr>
                <tr>
                    <td>Email: </td>
                    <td>{{ $user->email }}</td>
                </tr>
                <tr>
                    <td>Status: </td>
                    <td>
                        @if($user->active)
                            <span class="badge badge-success">Active</span>
                        @else
                            <span class="badge badge-danger">Inactive</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="text-center">
                        <a href="{{ route('logout') }}">Logout</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
@endsection
